
import { Eventing } from './Eventing';

export class Collection<T, K> {
  models: T[] = [];
  events: Eventing = new Eventing();

  constructor(
    public rootUrl: string,
    public deserialize: (json: K) => T
  ) {}

  get on() {
    return this.events.on;
  }

  get trigger() {
    return this.events.trigger;
  }

  fetch(): void {
    fetch(this.rootUrl)
      .then((response: Response): void => {
        if(!response.ok)
          throw new Error('Failed to fetch collection data from server.');

        response.json().then((collectionData: K[]): void => {
          collectionData.forEach((data): void => {
            this.models.push(this.deserialize(data));
          });

          this.trigger('change');
        });
      })
    ;
  }
}
