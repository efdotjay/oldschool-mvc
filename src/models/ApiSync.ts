
interface HasId {
  id?: number;
}

export class ApiSync<T extends HasId> {
  constructor(public rootUrl: string) {}

  fetch(id: number): Promise<T> {
    return fetch(`${this.rootUrl}/${id}`)
      .then((response: Response): Promise<T> => {
        if(!response.ok)
          throw new Error(`Failed to fetch data from server.`);

        return response.json();
      })
    ;
  }

  save(data: T): Promise<Response> {
    const { id } = data;

    if(id) {
      return fetch(`${this.rootUrl}/${id}`, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
      });
    }
    else {
      return fetch(this.rootUrl, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
      });
    }
  }
}
